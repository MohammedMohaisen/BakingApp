package com.winjitechnology.bakingapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.winjitechnology.bakingapp.Controller.RecipeController;
import com.winjitechnology.bakingapp.Model.Ingredient;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Muhammed Gamal on 11/22/2017.
 */

public class StepIngredients_Fragment extends Fragment {
    public static String STEP_INGREDIENTS_KEY_POSITION = "com.winjitechnology.bakingapp.step_ingredients_fragment.key_position";

    @BindView(R.id.step_ingredients_rv)
    RecyclerView mRecyclerView;

    private RecipeController mRecipeController;
    private int mPosition;
    private ArrayList mIngredientsList;
    private Unbinder unbinder;

    private RecyclerView.Adapter mAdapter;


    public static StepIngredients_Fragment newInstance(ArrayList<Ingredient> ingredients) {
        StepIngredients_Fragment ingredientFragment = new StepIngredients_Fragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("ingredients", ingredients);
        ingredientFragment.setArguments(bundle);

        return ingredientFragment;
    }



    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList("INGREDIENTS_AERIALIST", mIngredientsList);
        //save the current recycler view position
        outState.putParcelable("RECIPE_INGREDIENTS_BUNDLE", mRecyclerView.getLayoutManager().onSaveInstanceState());
        super.onSaveInstanceState(outState);
    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.step_ingredients_fragment, container, false);
        setRetainInstance(true);
        unbinder = ButterKnife.bind(this, view);

        mPosition = getArguments().getInt(STEP_INGREDIENTS_KEY_POSITION);

        mRecipeController = RecipeController.getInstance(getActivity(), "", null);

        mIngredientsList = (ArrayList) mRecipeController.getRecipeList().get(mPosition).getIngredients();

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(layoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(),
                layoutManager.getOrientation());
        mRecyclerView.addItemDecoration(dividerItemDecoration);

        mAdapter = new IngredientsAdapter(mIngredientsList);
        mRecyclerView.setAdapter(mAdapter);

        return view;
    }

    public class IngredientsAdapter extends RecyclerView.Adapter<IngredientsAdapter.IngredientsViewHolder> {

        private ArrayList mAdapterIngredientsList;

        public IngredientsAdapter(ArrayList ingredientsList) {
            mAdapterIngredientsList = ingredientsList;
        }

        @Override
        public IngredientsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            return new IngredientsViewHolder(layoutInflater, parent);
        }

        @Override
        public void onBindViewHolder(IngredientsViewHolder holder, int position) {
            holder.bind((Ingredient) mAdapterIngredientsList.get(position));
        }

        @Override
        public int getItemCount() {
            return mAdapterIngredientsList.size();
        }

        public class IngredientsViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.list_item_ingredient_name_tv)
            TextView mIngredientNameTextView;
            @BindView(R.id.list_item_ingredient_quantity_tv)
            TextView mIngredientQuantity;
            @BindView(R.id.list_item_ingredient_measure_tv)
            TextView mIngredientMeasure;

            public IngredientsViewHolder(LayoutInflater inflater, ViewGroup parent) {
                super(inflater.inflate(R.layout.list_item_ingredient, parent, false));
                ButterKnife.bind(this, itemView);
            }

            public void bind(Ingredient ingredient) {
                mIngredientNameTextView.setText(ingredient.getIngredient());
                mIngredientQuantity.setText(ingredient.getQuantity());
                mIngredientMeasure.setText(ingredient.getMeasure());

            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


}
