package com.winjitechnology.bakingapp;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Splash_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_);



        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent recAct = new Intent(Splash_Activity.this , Recipe_Activity.class);
                startActivity(recAct);
                finish();
            }
        }, 1500);


    }
}
