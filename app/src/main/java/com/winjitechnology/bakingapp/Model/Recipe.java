package com.winjitechnology.bakingapp.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by user on 8/7/2017.
 */

public class Recipe implements Parcelable {
    private String mId;
    private String mName;
    private List<Ingredient> mIngredients;
    private List<Step> mSteps;
    private String mServings;
    private String mImage;

    public Recipe(String id, String name, List<Ingredient> ingredients, List<Step> steps, String servings, String image) {
        mId = id;
        mName = name;
        mIngredients = ingredients;
        mSteps = steps;
        mServings = servings;
        mImage = image;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public List<Ingredient> getIngredients() {
        return mIngredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        mIngredients = ingredients;
    }

    public List<Step> getSteps() {
        return mSteps;
    }

    public void setSteps(List<Step> steps) {
        mSteps = steps;
    }

    public String getServings() {
        return mServings;
    }

    public void setServings(String servings) {
        mServings = servings;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }


    protected Recipe(Parcel in) {
        mId = in.readString();
        mServings = in.readString();
        mName = in.readString();
        mImage = in.readString();
        mIngredients = in.createTypedArrayList(Ingredient.CREATOR);
        mSteps = in.createTypedArrayList(Step.CREATOR);
    }

    public static final Creator<Recipe> CREATOR = new Creator<Recipe>() {
        @Override
        public Recipe createFromParcel(Parcel in) {
            return new Recipe(in);
        }

        @Override
        public Recipe[] newArray(int size) {
            return new Recipe[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mId);
        parcel.writeString(mServings);
        parcel.writeString(mName);
        parcel.writeString(mImage);
        parcel.writeTypedList(mIngredients);
        parcel.writeTypedList(mSteps);
    }







}
