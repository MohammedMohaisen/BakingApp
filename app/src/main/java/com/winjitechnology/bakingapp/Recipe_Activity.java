package com.winjitechnology.bakingapp;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.winjitechnology.bakingapp.Controller.ScreenSizeController;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Muhammed Gamal on 11/22/2017.
 */

public class Recipe_Activity extends AppCompatActivity {

    @BindView(R.id.recipe_activity_toolbar)
    Toolbar mToolBar;

    private boolean mIsTablet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recipe_activity);
        ButterKnife.bind(Recipe_Activity.this);

        //Toolbar Setup
        setSupportActionBar(mToolBar);

        //Check if this device is a tablet
        if (findViewById(R.id.recipe_container_tablet_left) != null)
            mIsTablet = true;

        homeClick();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (mIsTablet) {
            Fragment rightSideFragment = getSupportFragmentManager().findFragmentById(R.id.recipe_container_tablet_right);

            if (rightSideFragment instanceof Recipe_Fragment) {
                getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                        .replace(R.id.recipe_container_tablet_left, new Recipe_Tablet_Left_Fragment())
                        .commit();
            }
        }

    }

    @OnClick(R.id.app_bar_home)
    public void homeClick() {
        FragmentManager fm = getSupportFragmentManager();
        Fragment recipeFragment = new Recipe_Fragment();

        //If this device is a tablet, then load recipe fragment into tablet container
        if (mIsTablet) {
            ScreenSizeController.getInstance(this, mIsTablet, R.id.recipe_container_tablet_right);
            Fragment recipeTabletLeft = new Recipe_Tablet_Left_Fragment();
            fm.beginTransaction()
                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                    .replace(R.id.recipe_container_tablet_left, recipeTabletLeft)
                    .commitNow();

            fm.beginTransaction()
                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                    .replace(R.id.recipe_container_tablet_right, recipeFragment)
                    .commitNow();
            return;
        }

        //If this device is not a tablet, then load recipe fragment into phone container
        ScreenSizeController.getInstance(this, mIsTablet, R.id.recipe_container);

        //Load the recipe fragment
        fm.beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.recipe_container, recipeFragment)
                .commitNow();

    }

}
